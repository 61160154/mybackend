const express = require('express')
const router = express()

router.get('/', function (req, res, next) {
  res.json({ message: 'Hello Ployyyyyy' })
})

router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello', params })
})

module.exports = router
