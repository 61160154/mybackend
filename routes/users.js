const express = require('express')
const { route } = require('.')
const { userList } = require('../controller/UsersController')
const User = require('../models/User.js')
const router = express.Router()
const usersController = require('../controller/UsersController')

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

/* GET users listing. */
router.get('/', usersController.getUsers)

//get user by id
router.get('/:id', usersController.getUser)

//add new user
router.post('/', usersController.addUser)

//updateOne user by id
router.put('/', usersController.updateUser)

//deleteOne by id
router.delete('/:id', usersController.deleteUser)
module.exports = router
