const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect('mongodb://localhost:27017/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

//find from db
User.find(function (err, users) {
  if (err) return console.error(err)
  console.log(users)
  // console.log(users[1])
})

//this is method
// kittySchema.methods.speak = function () {
//   const greeting = this.name
// ? 'Meow name is ' + this.name
//     : "I don't have a name"
//   console.log(greeting)
// }
// const silence = new Kitten({ name: 'Silence' })
// console.log(silence.name)

// save in db
// const hus = new Kitten({ name: 'huskyyy' })
// bubi.speak()

// hus.save(function (err, cat) {
//   if (err) return console.error(err)
//   cat.speak()
// })
