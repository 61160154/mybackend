const User = require('../models/User.js')
const usersController = {
  userList: [
    { id: 1, name: 'Judy', gender: 'F' },
    { id: 2, name: 'Elle', gender: 'F' }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    //res.json(usersController.addUser(payload))

    console.log(payload)
    const user = new User(payload)
    try {
      newUser = await user.save()
      res.json(newUser)
    } catch (err) {
      res.status(500).send(err)
    }

    // User.create(payload)
    //   .then(user => {
    //     res.json(user)
    //   })
    //   .catch(err => {
    //     res.status(500).send(err)
    //   })
  },
  async updateUser (req, res, next) {
    const payload = req.body
    //res.json(usersController.updateUser(payload))

    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      //res.send(payload)
      res.send(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    //res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.send(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    //res.json(usersController.getUsers())

    //pattern1
    // User.find({}).exec(function (err, users) {
    //   if (err) {
    //     res.status(500).send()
    //   }
    //   res.json(users)
    // })

    //pattern2 Promise
    // User.find({})
    //   .then(function (users) {
    //     res.json(users)
    //   })
    //   .catch(function (err) {
    //     res.status(500).send(err)
    //   })

    //pattern3 Async Await ข้างในมี await ข้างนอกต้องมี async
    //router.get('/', async (req, res, next) => {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    //res.json(usersController.getUser(id))

    //Pattern2 Promise
    // User.findById(id)
    //   .then(function (user) {
    //     res.json(user)
    //   })
    //   .catch(err => {
    //     res.status(500).send(err)
    //   })

    //Pattern3 Async Await
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = usersController
